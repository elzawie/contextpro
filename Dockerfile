FROM ubuntu:20.04

RUN apt-get update \
    && apt-get install -qy software-properties-common \
    && apt-get update \
    && add-apt-repository ppa:deadsnakes/ppa

RUN apt-get install -qy python3.6 python3.7 python3.8 python3.9 python3-pip python3.6-dev libpython3.7-dev libpython3.8-dev libpython3.9-dev

# Install poetry, nox, flake8 and mypy globally
RUN pip3 install toml poetry nox nox-poetry flake8 mypy coverage