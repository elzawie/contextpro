import os

import pandas as pd

TRAIN_POS_REVIEWS_PATH = "data/aclImdb/train/pos"
TRAIN_NEG_REVIEWS_PATH = "data/aclImdb/train/neg"
TEST_POS_REVIEWS_PATH = "data/aclImdb/test/pos"
TEST_NEG_REVIEWS_PATH = "data/aclImdb/test/neg"


def read_txt_file(path: str):
    if not path.endswith(".txt"):
        raise ValueError("Provided path does not point to a '.txt' document")
    with open(path, mode="r") as file:
        return file.read()


def read_txt_files_from_directory(dir_path: str):
    dataset = []
    for file in os.listdir(dir_path):
        filepath = os.path.join(dir_path, file)
        if not filepath.endswith(".txt"):
            continue
        dataset.append(read_txt_file(filepath))
    return dataset


# Read all .txt files
train_pos_revs = read_txt_files_from_directory(TRAIN_POS_REVIEWS_PATH)
train_neg_revs = read_txt_files_from_directory(TRAIN_NEG_REVIEWS_PATH)
test_pos_revs = read_txt_files_from_directory(TEST_POS_REVIEWS_PATH)
test_neg_revs = read_txt_files_from_directory(TEST_NEG_REVIEWS_PATH)

# Construct pandas DataFrame objects for each dataset
# and one which combines all of them
train_pos_df = pd.DataFrame({"text": train_pos_revs, "label": 1})
train_neg_df = pd.DataFrame({"text": train_neg_revs, "label": 0})
test_pos_df = pd.DataFrame({"text": test_pos_revs, "label": 1})
test_neg_df = pd.DataFrame({"text": test_neg_revs, "label": 0})
combined_reviews_df = pd.concat([train_pos_df, train_neg_df, test_pos_df, test_neg_df])

# Save datasets as .xlsx files
train_pos_df.to_excel(
    TRAIN_POS_REVIEWS_PATH + "/" + "train_pos.xlsx", index=False, engine="xlsxwriter"
)
train_neg_df.to_excel(
    TRAIN_NEG_REVIEWS_PATH + "/" + "train_neg.xlsx", index=False, engine="xlsxwriter"
)
test_pos_df.to_excel(
    TEST_POS_REVIEWS_PATH + "/" + "test_pos.xlsx", index=False, engine="xlsxwriter"
)
test_neg_df.to_excel(
    TEST_NEG_REVIEWS_PATH + "/" + "test_neg.xlsx", index=False, engine="xlsxwriter"
)
combined_reviews_df.to_excel(
    "data/aclImdb" + "/" + "combined_reviews.xlsx", index=False, engine="xlsxwriter"
)
