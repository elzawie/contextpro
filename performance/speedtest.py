import timeit

import contextpro as ctp
import pandas as pd

# Load first dataset - movie reviews
reviews = pd.read_excel("performance/data/aclImdb/combined_reviews.xlsx")

# Remove 2 invalid reviews which were replaced with int 0
# while reading the dataset into the memory
reviews = reviews.drop([21283, 42241])

# Tokenize sentences
reviews["tokens"] = reviews["text"].apply(lambda x: x.split())

# -----------------------------------------------------------------------------------

# Test stemming in for loop
stmt = """
results = []

for token_list in reviews["tokens"].tolist():
    results.append(ctp.stem(token_list))
"""
exec_time = timeit.repeat(stmt=stmt, repeat=10, number=1, globals=globals())
print(f"Stem in for loop, best execution took: {min(exec_time)} seconds" + "\n")


# Test stemming using list comprehension
stmt = """
results = [ctp.stem(token_list) for token_list in reviews["tokens"].tolist()]
"""
exec_time = timeit.repeat(stmt=stmt, repeat=10, number=1, globals=globals())

print(
    (
        "Stem using list comprehension, "
        f"best execution took: {min(exec_time)} seconds" + "\n"
    )
)

# Test stemming using pd.Series apply method
stmt = """
reviews["tokens_stemmed"] = reviews["tokens"].apply(lambda x: ctp.stem(x))
"""

exec_time = timeit.repeat(stmt=stmt, repeat=10, number=1, globals=globals())
print(
    (
        "Stem using pd.Series apply method, "
        f"best execution took: {min(exec_time)} seconds" + "\n"
    )
)

# Test stemming with contextpro
stmt = """
results = ctp.batch_stem(reviews["tokens"].tolist())
"""

exec_time = timeit.repeat(stmt=stmt, repeat=10, number=1, globals=globals())
print(f"Stem using contextpro, best execution took: {min(exec_time)} seconds" + "\n")

# -----------------------------------------------------------------------------------

# Test lemmatization in for loop
stmt = """
results = []

for token_list in reviews["tokens"].tolist():
    results.append(ctp.lemmatize(token_list))
"""
exec_time = timeit.repeat(stmt=stmt, repeat=10, number=1, globals=globals())
print(f"Lemmatize in for loop, best execution took: {min(exec_time)} seconds" + "\n")


# Test lemmatization using list comprehension
stmt = """
results = [ctp.lemmatize(token_list) for token_list in reviews["tokens"].tolist()]
"""
exec_time = timeit.repeat(stmt=stmt, repeat=10, number=1, globals=globals())

print(
    (
        "Lemmatize using list comprehension, "
        f"best execution took: {min(exec_time)} seconds" + "\n"
    )
)

# Test lemmatization using pd.Series apply method
stmt = """
reviews["tokens_lemmatized"] = reviews["tokens"].apply(lambda x: ctp.lemmatize(x))
"""

exec_time = timeit.repeat(stmt=stmt, repeat=10, number=1, globals=globals())
print(
    (
        "Lemmatize using pd.Series apply method, "
        f"best execution took: {min(exec_time)} seconds" + "\n"
    )
)

# Test lemmatization with contextpro
stmt = """
results = ctp.batch_lemmatize(reviews["tokens"].tolist())
"""

exec_time = timeit.repeat(stmt=stmt, repeat=10, number=1, globals=globals())
print(
    (
        "Lemmatize using contextpro, "
        f"best execution took: {min(exec_time)} seconds" + "\n"
    )
)

# -----------------------------------------------------------------------------------

# Test sentiment scores calculation in for loop
stmt = """
results = []

for text in reviews["text"].tolist():
    results.append(ctp.calculate_sentiment_score(text))
"""
exec_time = timeit.repeat(stmt=stmt, repeat=10, number=1, globals=globals())
print(
    (
        "Calculate sentiment scores in for loop, best execution "
        f"took: {min(exec_time)} seconds" + "\n"
    )
)

# Test sentiment scores calculation using list comprehension
stmt = """
results = [ctp.calculate_sentiment_score(text) for text in reviews["text"].tolist()]
"""
exec_time = timeit.repeat(stmt=stmt, repeat=10, number=1, globals=globals())

print(
    (
        "Calculate sentiment scores using list comprehension, "
        f"best execution took: {min(exec_time)} seconds" + "\n"
    )
)

# Test sentiment scores calculation using pd.Series apply method
stmt = """
reviews["sentiment"] = reviews["text"].apply(lambda x: ctp.calculate_sentiment_score(x))
"""

exec_time = timeit.repeat(stmt=stmt, repeat=10, number=1, globals=globals())
print(
    (
        "Calculate sentiment scores using pd.Series apply method, "
        f"best execution took: {min(exec_time)} seconds" + "\n"
    )
)

# Test sentiment scores calculation with contextpro
stmt = """
results = ctp.batch_calculate_sentiment_scores(reviews["text"].tolist())
"""

exec_time = timeit.repeat(stmt=stmt, repeat=10, number=1, globals=globals())
print(
    (
        "Calculate sentiment scores using contextpro, "
        f"best execution took: {min(exec_time)} seconds" + "\n"
    )
)

# -----------------------------------------------------------------------------------

# Test subjectivity scores calculation in for loop
stmt = """
results = []

for text in reviews["text"].tolist():
    results.append(ctp.calculate_subjectivity_score(text))
"""
exec_time = timeit.repeat(stmt=stmt, repeat=10, number=1, globals=globals())
print(
    (
        "Calculate subjectivity scores in for loop, best execution "
        f"took: {min(exec_time)} seconds" + "\n"
    )
)

# Test subjectivity scores calculation using list comprehension
stmt = """
results = [ctp.calculate_subjectivity_score(text) for text in reviews["text"].tolist()]
"""
exec_time = timeit.repeat(stmt=stmt, repeat=10, number=1, globals=globals())

print(
    (
        "Calculate subjectivity scores using list comprehension, "
        f"best execution took: {min(exec_time)} seconds" + "\n"
    )
)

# Test subjectivity scores calculation using pd.Series apply method
stmt = """
reviews["subjectivity"] = reviews["text"].apply(lambda x: ctp.calculate_subjectivity_score(x))
"""

exec_time = timeit.repeat(stmt=stmt, repeat=10, number=1, globals=globals())
print(
    (
        "Calculate subjectivity scores using pd.Series apply method, "
        f"best execution took: {min(exec_time)} seconds" + "\n"
    )
)

# Test subjectivity scores calculation with contextpro
stmt = """
results = ctp.batch_calculate_subjectivity_scores(reviews["text"].tolist())
"""

exec_time = timeit.repeat(stmt=stmt, repeat=10, number=1, globals=globals())
print(
    (
        "Calculate subjectivity scores using contextpro, "
        f"best execution took: {min(exec_time)} seconds" + "\n"
    )
)

# -----------------------------------------------------------------------------------

# Test converting numerals to numbers in for loop
stmt = """
results = []

for text in reviews["text"].tolist():
    results.append(ctp.convert_numerals_to_numbers(text))
"""
exec_time = timeit.repeat(stmt=stmt, repeat=10, number=1, globals=globals())
print(
    (
        "Convert numerals to numbers in for loop, best execution "
        f"took: {min(exec_time)} seconds" + "\n"
    )
)


# Test converting numerals to numbers using list comprehension
stmt = """
results = [ctp.convert_numerals_to_numbers(text) for text in reviews["text"].tolist()]
"""
exec_time = timeit.repeat(stmt=stmt, repeat=10, number=1, globals=globals())

print(
    (
        "Convert numerals to numbers using list comprehension, "
        f"best execution took: {min(exec_time)} seconds" + "\n"
    )
)

# Test converting numerals to numbers using pd.Series apply method
stmt = """
reviews["text_numerals_as_numbers"] = reviews["text"].apply(lambda x: ctp.convert_numerals_to_numbers(x))
"""

exec_time = timeit.repeat(stmt=stmt, repeat=10, number=1, globals=globals())
print(
    (
        "Convert numerals to numbers using pd.Series apply method, "
        f"best execution took: {min(exec_time)} seconds" + "\n"
    )
)

# Test converting numerals to numbers with contextpro
stmt = """
results = ctp.batch_convert_numerals_to_numbers(reviews["text"].tolist())
"""

exec_time = timeit.repeat(stmt=stmt, repeat=10, number=1, globals=globals())
print(
    (
        "Convert numerals to numbers using contextpro, best execution "
        f"took: {min(exec_time)} seconds" + "\n"
    )
)

# -----------------------------------------------------------------------------------

# Test tokenization in for loop
stmt = """
results = []

for text in reviews["text"].tolist():
    results.append(ctp.tokenize_text(text))
"""
exec_time = timeit.repeat(stmt=stmt, repeat=10, number=1, globals=globals())
print(
    (
        "Tokenize text in for loop, best execution "
        f"took: {min(exec_time)} seconds" + "\n"
    )
)


# Test tokenization using list comprehension
stmt = """
results = [ctp.tokenize_text(text) for text in reviews["text"].tolist()]
"""
exec_time = timeit.repeat(stmt=stmt, repeat=10, number=1, globals=globals())

print(
    (
        "Tokenize text using list comprehension, "
        f"best execution took: {min(exec_time)} seconds" + "\n"
    )
)

# Test tokenization using pd.Series apply method
stmt = """
reviews["text_tokens"] = reviews["text"].apply(lambda x: ctp.tokenize_text(x))
"""

exec_time = timeit.repeat(stmt=stmt, repeat=10, number=1, globals=globals())
print(
    (
        "Tokenize text using pd.Series apply method, "
        f"best execution took: {min(exec_time)} seconds" + "\n"
    )
)

# Test tokenization with contextpro
stmt = """
results = ctp.batch_tokenize_text(reviews["text"].tolist())
"""

exec_time = timeit.repeat(stmt=stmt, repeat=10, number=1, globals=globals())
print(
    (
        "Tokenize text using contextpro, best execution "
        f"took: {min(exec_time)} seconds" + "\n"
    )
)
