from contextpro.normalization import batch_replace_contractions
from contextpro.tokenization import batch_tokenize_text

documents = [
    "I don't want to be rude, but you shouldn't do this",
    "Do you think he'll pass his driving test?",
    "I'll see you next week",
    "I'm going for a walk",
    "I'm hungry - can we eat now, please?",
    "It's freezing outside!",
    "I wish she'd told me she wasn’t coming.",
    "Do you think he’d like to come to our party?",
    "Why aren't you answering your phone?",
    "I can't find my glasses anywhere.",
    "They didn't tell me the meeting was cancelled.",
    "He hasn't been in touch for over a month.",
    "You mustn't be late for work",
    "I shouldn't have eaten so much!",
    "My husband's been feeling ill for days, but he won't go to the doctor.",
    "My dad wouldn't let me drive his car.",
    "What's the time?",
    "Where's my newspaper?",
    "When's your wedding?",
    "Who's coming to your wedding?",
]


sentences_without_contractions = batch_replace_contractions(documents, num_workers=8)

tokens = batch_tokenize_text(
    sentences_without_contractions,
    tokenizer_method="nltk_word_tokenizer",
    num_workers=8,
)
