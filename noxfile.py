from nox_poetry import session


@session
def lint(session):
    session.install(".")
    session.run(
        "flake8",
        "--max-line-length",
        "88",
        "--extend-ignore",
        "E203",
        "--filename",
        "*.py",
        "contextpro/",
    )


@session
def check_types(session):
    session.install(".")
    session.run("mypy", "contextpro", "tests")


@session(python=["3.6", "3.7", "3.8", "3.9"])
def test_python(session):
    session.install(".")
    session.run("python", "install_dependencies.py")
    session.run("python", "-m", "spacy", "download", "en_core_web_sm")
    session.run("python", "-m" "unittest")


@session
def test_coverage(session):
    session.install(".")
    session.install("coverage")
    session.run("python", "install_dependencies.py")
    session.run("python", "-m", "spacy", "download", "en_core_web_sm")
    session.run("coverage", "run", "-m", "unittest")
    session.run("coverage", "report")
