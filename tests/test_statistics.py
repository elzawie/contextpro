"""This module contains test methods for functions from the
'contextpro.statistics.py' module."""

import json
import os
import unittest

import pandas as pd

from contextpro.statistics import (
    batch_calculate_corpus_statistics,
    batch_calculate_sentiment_scores,
    batch_calculate_subjectivity_scores,
    batch_get_ngram_counts,
    calculate_sentiment_score,
    calculate_subjectivity_score,
    get_ngram_counts,
)

TEST_DATA_DIR_PATH = os.path.dirname(os.path.abspath(__file__)) + "/" + "data"
SENTENCES_WITH_CONTRACTIONS = (
    TEST_DATA_DIR_PATH + "/" + "sentences_with_contractions.json"
)


class TestStatistics(unittest.TestCase):
    def test_get_ngram_counts_unigrams(self):
        """Calculates unigram counts in a document."""
        tokens = ["my", "name", "is", "dr", "jekyll"]
        expected_result = {"my": 1, "name": 1, "is": 1, "dr": 1, "jekyll": 1}
        counts = get_ngram_counts(tokens, ngram_size=1)
        self.assertEqual(expected_result, counts)

    def test_get_ngram_counts_bigrams(self):
        """Calculates bigram counts in a document."""
        tokens = ["my", "name", "is", "dr", "jekyll"]
        expected_result = {"my name": 1, "name is": 1, "is dr": 1, "dr jekyll": 1}
        counts = get_ngram_counts(tokens, ngram_size=2)
        self.assertEqual(expected_result, counts)

    def test_get_batch_ngram_counts_unigrams(self):
        """Calculates unigram counts in a corpus of tokenized
        documents.
        """
        corpus = [
            ["my", "name", "is", "dr", "jekyll"],
            ["his", "name", "is", "mr", "hyde"],
            ["this", "guy", "name", "is", "edward", "scissorhands"],
            ["and", "this", "is", "tom", "parker"],
        ]
        expected_result = {
            "my": 1,
            "name": 3,
            "is": 4,
            "dr": 1,
            "jekyll": 1,
            "his": 1,
            "mr": 1,
            "hyde": 1,
            "this": 2,
            "guy": 1,
            "edward": 1,
            "scissorhands": 1,
            "and": 1,
            "tom": 1,
            "parker": 1,
        }

        counts = batch_get_ngram_counts(corpus, ngram_size=1)
        self.assertEqual(expected_result, counts)

    def test_get_batch_ngram_counts_bigrams(self):
        """Calculates bigram counts in a corpus of tokenized
        documents.
        """
        corpus = [
            ["my", "name", "is", "dr", "jekyll"],
            ["his", "name", "is", "mr", "hyde"],
            ["this", "guy", "name", "is", "edward", "scissorhands"],
            ["and", "this", "is", "tom", "parker"],
        ]
        expected_result = {
            "my name": 1,
            "name is": 3,
            "is dr": 1,
            "dr jekyll": 1,
            "his name": 1,
            "is mr": 1,
            "mr hyde": 1,
            "this guy": 1,
            "guy name": 1,
            "is edward": 1,
            "edward scissorhands": 1,
            "and this": 1,
            "this is": 1,
            "is tom": 1,
            "tom parker": 1,
        }
        counts = batch_get_ngram_counts(corpus, ngram_size=2)
        self.assertEqual(expected_result, counts)

    def test_calculate_sentiment_score(self):
        """Calculates the sentiment score for the sentence"""

        sentence = "I don't want to be rude, but you shouldn't do this"
        expected_result = -0.3
        result = calculate_sentiment_score(sentence)
        self.assertEqual(expected_result, result)

    def test_calculate_subjectivity_score(self):
        """Calculates the subjectivity score for the sentence"""

        sentence = "I don't want to be rude, but you shouldn't do this"
        expected_result = 0.6
        result = calculate_subjectivity_score(sentence)
        self.assertEqual(expected_result, result)

    def test_batch_calculate_sentiment_scores_raises_ValueError(self):
        """Tests that the ValueError is raised on invalid input."""
        with self.assertRaises(ValueError):
            sentence_list = [1, "Here we go again"]
            batch_calculate_sentiment_scores(sentence_list)

    def test_batch_calculate_subjectivity_scores_raises_ValueError(self):
        """Tests that the ValueError is raised on invalid input."""
        with self.assertRaises(ValueError):
            sentence_list = [1, "Here we go again"]
            batch_calculate_subjectivity_scores(sentence_list)

    def test_batch_get_ngram_counts_raises_ValueError(self):
        """Tests that the ValueError is raised on invalid input."""
        with self.assertRaises(ValueError):
            sentence_list = [1, "Here we go again"]
            batch_get_ngram_counts(sentence_list)

    def test_batch_calculate_corpus_statistics_raises_ValueError(self):
        """Tests that the ValueError is raised on invalid input."""
        with self.assertRaises(ValueError):
            sentence_list = [1, "Here we go again"]
            batch_calculate_corpus_statistics(sentence_list)

    def test_batch_calculate_sentiment_scores(self):
        """Processes the sentences in a concurrent manner and calculates the
        sentiment scores for them"""
        with open(SENTENCES_WITH_CONTRACTIONS, encoding="utf-8") as f1:
            sentences = json.load(f1)

        expected_result = [
            -0.3,
            0.0,
            0.0,
            0.0,
            0.0,
            0.0,
            0.0,
            0.0,
            0.0,
            0.0,
            0.0,
            0.0,
            -0.3,
            0.25,
            -0.5,
            0.0,
            0.0,
            0.0,
            0.0,
            0.0,
        ]

        result = batch_calculate_sentiment_scores(sentences, num_workers=2)
        self.assertEqual(expected_result, result)

    def test_batch_calculate_subjectivity_scores(self):
        """Processes the sentences in a concurrent manner and calculates the
        subjectivity scores for them"""
        with open(SENTENCES_WITH_CONTRACTIONS, encoding="utf-8") as f1:
            sentences = json.load(f1)

        expected_result = [
            0.6,
            0.0,
            0.0,
            0.0,
            0.0,
            0.05,
            0.0,
            0.0,
            0.0,
            0.0,
            0.0,
            0.0,
            0.6,
            0.2,
            1.0,
            0.0,
            0.0,
            0.0,
            0.0,
            0.0,
        ]

        result = batch_calculate_subjectivity_scores(sentences, num_workers=2)
        self.assertEqual(expected_result, result)

    def test_batch_calculate_corpus_statistics(self):
        """Processes the sentences in a concurrent manner, calculates
        the below statistics for them and outputs them as a pandas
        DataFrame.

            - Number of characters
            - Number of tokens
            - Number of punctuation characters
            - Number of digits
            - Number of whitespace characters
            - Number of non-ascii characters
            - Sentiment score
            - Subjectivity score
        """
        with open(SENTENCES_WITH_CONTRACTIONS, encoding="utf-8") as f1:
            sentences = json.load(f1)

        result = batch_calculate_corpus_statistics(
            sentences,
            lowercase=False,
            remove_stopwords=False,
            num_workers=2,
        )

        self.assertIsInstance(result, pd.DataFrame)
        self.assertEqual((20, 8), result.shape)

    def test_batch_calculate_corpus_statistics_lowercased_custom_stopwords(self):
        """Processes the sentences in a concurrent manner by lowercasing
        the characters and removing custom stopwords and calculates
        the below statistics for them and outputs them as a pandas
        DataFrame.

            - Number of characters
            - Number of tokens
            - Number of punctuation characters
            - Number of digits
            - Number of whitespace characters
            - Number of non-ascii characters
            - Sentiment score
            - Subjectivity score
        """
        with open(SENTENCES_WITH_CONTRACTIONS, encoding="utf-8") as f1:
            sentences = json.load(f1)

        result = batch_calculate_corpus_statistics(
            sentences,
            lowercase=True,
            remove_stopwords=True,
            custom_stopwords=["me", "you", "he", "she", "not", "and", "or"],
            tokenizer_pattern=r"\b[0-9]+\b",
            num_workers=2,
        )

        self.assertIsInstance(result, pd.DataFrame)
        self.assertEqual((20, 8), result.shape)
