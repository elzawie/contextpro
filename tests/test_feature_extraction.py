"""This module contains test methods for classes and functions
from the 'contextpro.feature_extraction.py' module."""

import unittest

from contextpro.feature_extraction import batch_get_ngrams, get_ngrams


class TestFeatureExtraction(unittest.TestCase):
    def test_get_ngrams_unigrams(self):
        """Test that 'get_ngrams(ngram_size=1)' function outputs list of
        unigrams.
        """
        tokens = ["my", "name", "is", "spiderman"]
        expected_result = ["my", "name", "is", "spiderman"]
        result = get_ngrams(tokens, ngram_size=1)
        self.assertEqual(expected_result, result)

    def test_get_ngrams_bigrams(self):
        """Test that 'get_ngrams(ngram_size=2)' function outputs list of
        bigrams.
        """
        tokens = ["my", "name", "is", "spiderman"]
        expected_result = ["my name", "name is", "is spiderman"]
        result = get_ngrams(tokens, ngram_size=2)
        self.assertEqual(expected_result, result)

    def test_get_ngrams_trigrams(self):
        """Test that 'get_ngrams(ngram_size=3)' function outputs list of
        trigrams.
        """
        tokens = ["my", "name", "is", "spiderman"]
        expected_result = ["my name is", "name is spiderman"]
        result = get_ngrams(tokens, ngram_size=3)
        self.assertEqual(expected_result, result)

    def test_batch_get_ngrams_bigrams(self):
        """Test that 'batch_get_ngrams(ngram_size=2)' function outputs
        a list of nested bigram lists.
        """
        tokens = [
            ["my", "name", "is", "spiderman"],
            ["she", "lives", "in", "australia"],
        ]
        expected_result = [
            ["my name", "name is", "is spiderman"],
            ["she lives", "lives in", "in australia"],
        ]
        result = batch_get_ngrams(tokens, ngram_size=2)
        self.assertEqual(expected_result, result)
