"""This module contains test methods for functions from the
'contextpro.tokenization.py' module."""

import json
import os
import unittest

from contextpro.tokenization import batch_tokenize_text, tokenize_text

TEST_DATA_DIR_PATH = os.path.dirname(os.path.abspath(__file__)) + "/" + "data"

SENTENCES_WITHOUT_CONTRACTIONS = (
    TEST_DATA_DIR_PATH + "/" + "sentences_without_contractions.json"
)
TOKENS_WORD_TOKENIZER = TEST_DATA_DIR_PATH + "/" + "tokens_word_tokenizer.json"
TOKENS_REGEXP_TOKENIZER = TEST_DATA_DIR_PATH + "/" + "tokens_regexp_tokenizer.json"


class TestTokenization(unittest.TestCase):
    def test_tokenize_text_word_tokenizer(self):
        """Tests that the NLTK's word_tokenize() method is able to
        tokenize the sentence."""

        text = "I do not want to be rude, but you should not do this"
        expected_result = [
            "I",
            "do",
            "not",
            "want",
            "to",
            "be",
            "rude",
            ",",
            "but",
            "you",
            "should",
            "not",
            "do",
            "this",
        ]
        result = tokenize_text(text)
        self.assertEqual(expected_result, result)

    def test_tokenize_text_regexp_tokenizer_split_by_spaces(self):
        """Tests that the NLTK's regexp_tokenize() method is able to
        tokenize the sentence by splitting it on space characters"""

        text = "I do not want to be rude, but you should not do this"
        expected_result = [
            "I",
            "do",
            "not",
            "want",
            "to",
            "be",
            "rude,",
            "but",
            "you",
            "should",
            "not",
            "do",
            "this",
        ]
        result = tokenize_text(
            text, tokenizer_method="nltk_regexp_tokenizer", pattern=r"\s+", gaps=True
        )
        self.assertEqual(expected_result, result)

    def test_tokenize_text_regexp_tokenizer_words_only(self):
        """Tests that the NLTK's regexp_tokenize() method is able to
        tokenize the sentence leaving only the words."""

        text = """My name is Tomlino, I am 20 years old. My post office
        box number is 9054 Haslen, Switzerland.
        """
        expected_result = [
            "My",
            "name",
            "is",
            "Tomlino",
            "I",
            "am",
            "years",
            "old",
            "My",
            "post",
            "office",
            "box",
            "number",
            "is",
            "Haslen",
            "Switzerland",
        ]
        result = tokenize_text(
            text,
            tokenizer_method="nltk_regexp_tokenizer",
            pattern=r"\b[^\d\W]+\b",
            gaps=False,
        )
        self.assertEqual(expected_result, result)

    def test_batch_tokenize_raises_ValueError(self):
        """Tests that the ValueError is raised on invalid input."""
        with self.assertRaises(ValueError):
            sentence_list = [1, "Here we go again"]
            batch_tokenize_text(sentence_list)

    def test_batch_tokenize_text_word_tokenizer(self):
        """Processes the sentences in a concurrent manner and tokenizes
        them using NLTK's word_tokenize() method."""

        with open(SENTENCES_WITHOUT_CONTRACTIONS, encoding="utf-8") as f:
            sentence_list = json.load(f)
        with open(TOKENS_WORD_TOKENIZER, encoding="utf-8") as f:
            expected_result = json.load(f)
        result = batch_tokenize_text(
            sentence_list, tokenizer_method="nltk_word_tokenizer"
        )
        self.assertEqual(expected_result, result)

    def test_batch_tokenize_text_regexp_toknizer_words_only(self):
        """Processes the sentences in a concurrent manner and tokenizes
        them using NLTK's regexp_tokenize() method. Removes numbers,
        punctuation marks and other special characters."""

        with open(SENTENCES_WITHOUT_CONTRACTIONS, encoding="utf-8") as f:
            sentence_list = json.load(f)
        with open(TOKENS_REGEXP_TOKENIZER, encoding="utf-8") as f:
            expected_result = json.load(f)
        result = batch_tokenize_text(
            sentence_list,
            tokenizer_method="nltk_regexp_tokenizer",
            pattern=r"\b[^\d\W]+\b",
            gaps=False,
        )
        self.assertEqual(expected_result, result)
