"""This module contains test methods for functions from the
'contextpro.normalization.py' module."""

import json
import os
import unittest

from contextpro.normalization import (
    batch_convert_numerals_to_numbers,
    batch_lemmatize,
    batch_lowercase_text,
    batch_remove_non_ascii_characters,
    batch_remove_numbers,
    batch_remove_punctuation,
    batch_remove_stopwords,
    batch_remove_whitespace,
    batch_replace_contractions,
    batch_stem,
    convert_numerals_to_numbers,
    lemmatize,
    remove_non_ascii_characters,
    remove_numbers,
    remove_punctuation,
    remove_stopwords,
    remove_whitespace,
    replace_contractions,
    stem,
)

TEST_DATA_DIR_PATH = os.path.dirname(os.path.abspath(__file__)) + "/" + "data"
SENTENCES_TITLECASE = TEST_DATA_DIR_PATH + "/" + "sentences_titlecase.json"
SENTENCES_LOWERCASE = TEST_DATA_DIR_PATH + "/" + "sentences_lowercase.json"
SENTENCES_WITH_NUMBERS = TEST_DATA_DIR_PATH + "/" + "sentences_with_numbers.json"
SENTENCES_WITH_NUMERALS = TEST_DATA_DIR_PATH + "/" + "sentences_with_numerals.json"
SENTENCES_WITH_WHITESPACE = TEST_DATA_DIR_PATH + "/" + "sentences_with_whitespace.json"
SENTENCES_WITHOUT_NUMBERS = TEST_DATA_DIR_PATH + "/" + "sentences_without_numbers.json"
SENTENCES_WITHOUT_NUMERALS = (
    TEST_DATA_DIR_PATH + "/" + "sentences_with_converted_numerals.json"
)

SENTENCES_WITHOUT_WHITESPACE = (
    TEST_DATA_DIR_PATH + "/" + "sentences_without_whitespace.json"
)
SENTENCES_WITH_CONTRACTIONS = (
    TEST_DATA_DIR_PATH + "/" + "sentences_with_contractions.json"
)
SENTENCES_WITHOUT_CONTRACTIONS = (
    TEST_DATA_DIR_PATH + "/" + "sentences_without_contractions.json"
)
SENTENCES_TITLECASE_NO_PUNCT = (
    TEST_DATA_DIR_PATH + "/" + "sentences_titlecase_stripped_punctuation.json"
)

TOKENS_LOWERCASE = TEST_DATA_DIR_PATH + "/" + "tokens_lowercase.json"
TOKENS_WITHOUT_STOPWORDS = TEST_DATA_DIR_PATH + "/" + "tokens_without_stopwords.json"
TOKENS_STEMMED = TEST_DATA_DIR_PATH + "/" + "tokens_lowercase_stemmed.json"
TOKENS_LEMMATIZED = TEST_DATA_DIR_PATH + "/" + "tokens_lowercase_lemmatized.json"


class TestNormalization(unittest.TestCase):
    def test_remove_numbers(self):
        """Tests that numbers are removed from the sentence."""
        sentence = "I have 5 cars in 3 garages."
        expected_result = "I have  cars in  garages."
        result = remove_numbers(sentence)
        self.assertEqual(expected_result, result)

    def test_remove_punctuation(self):
        """Tests that punctuation characters are removed from the sentence."""
        sentence = "You should not do this!!!"
        expected_result = "You should not do this"
        result = remove_punctuation(sentence)
        self.assertEqual(expected_result, result)

    def test_remove_whitespace(self):
        """Tests that whitespace characters are removed from the sentence."""
        sentence = "I Will See \r\nYou next Week"
        expected_result = "I Will See You next Week"
        result = remove_whitespace(sentence)
        self.assertEqual(expected_result, result)

    def test_convert_numerals_to_numbers(self):
        """Tests that numerals are converted to numbers."""
        sentence = "I have five cars and two garages"
        expected_result = "I have 5 cars and 2 garages"
        result = convert_numerals_to_numbers(sentence)
        self.assertEqual(expected_result, result)

    def test_remove_stopwords(self):
        tokens = ["my", "name", "is", "tom", "i", "live", "in", "london"]
        expected_result = ["name", "tom", "live", "london"]
        result = remove_stopwords(tokens)
        self.assertEqual(expected_result, result)

    def test_remove_non_ascii_characters(self):
        """Tests that all non-ascii characters are removed from sentence"""
        sentence = "this time air\u00e6\u00e3 was filled\u00a3"
        expected_result = "this time air was filled"
        result = remove_non_ascii_characters(sentence)
        self.assertEqual(expected_result, result)

    def test_replace_contractions(self):
        """Tests that the contractions are properly expanded."""

        sentence = "I don't want to be rude, but you shouldn't do this"
        expected_result = "I do not want to be rude, but you should not do this"
        result = replace_contractions(sentence)
        self.assertEqual(expected_result, result)

    def test_stem_porter(self):
        """Tests that all words are reduced to their root form using
        NLTK's PorterStemmer"""

        tokens = ["wait", "waiting", "waited", "waits"]
        expected_result = ["wait", "wait", "wait", "wait"]
        result = stem(tokens, stemmer_type="nltk_porter_stemmer")
        self.assertEqual(expected_result, result)

    def test_stem_lancaster(self):
        """Tests that all words are reduced to their root form using
        NLTK's LancasterStemmer"""

        tokens = ["wait", "waiting", "waited", "waits"]
        expected_result = ["wait", "wait", "wait", "wait"]
        result = stem(tokens, stemmer_type="nltk_lancaster_stemmer")
        self.assertEqual(expected_result, result)

    def test_stem_snowball(self):
        """Tests that all words are reduced to their root form using
        NLTK's SnowballStemmer"""

        tokens = ["wait", "waiting", "waited", "waits"]
        expected_result = ["wait", "wait", "wait", "wait"]
        result = stem(tokens, stemmer_type="nltk_snowball_stemmer", language="english")
        self.assertEqual(expected_result, result)

    def test_stem_regexp(self):
        """Tests that all words are reduced to their root form using
        NLTK's RegexpStemmer"""

        tokens = ["cars", "wait", "waiting", "waited", "waits"]
        expected_result = ["cars", "wait", "wait", "waited", "wait"]
        result = stem(
            tokens, stemmer_type="nltk_regexp_stemmer", regexp="ing$|s$|e$|able$", min=5
        )
        self.assertEqual(expected_result, result)

    def test_lemmatize_wordnet(self):
        """Tests that all words are lemmatized using NLTK'S WordNetLemmatizer"""
        tokens = ["rocks", "corpora", "dogs", "abaci", "cars", "languages"]
        expected_result = ["rock", "corpus", "dog", "abacus", "car", "language"]
        result = lemmatize(tokens)
        self.assertEqual(expected_result, result)

    def test_lemmatize_wordnet_with_pos_tag(self):
        """Tests that only 'adjectives' are lemmatized using NLTK'S WordNetLemmatizer"""
        tokens = [
            "cars",
            "corpora",
            "dogs",
            "better",
            "littler",
            "worse",
            "worst",
            "lighter",
            "darker",
            "lightest",
        ]
        expected_result = [
            "cars",
            "corpora",
            "dogs",
            "good",
            "little",
            "bad",
            "bad",
            "light",
            "dark",
            "light",
        ]
        result = lemmatize(tokens, pos="a")
        self.assertEqual(expected_result, result)

    def test_batch_lowercase_raises_ValueError(self):
        """Tests that the ValueError is raised on invalid input."""
        with self.assertRaises(ValueError):
            sentence_list = [1, "Here we go again"]
            batch_lowercase_text(sentence_list)

    def test_batch_remove_non_ascii_characters_raises_ValueError(self):
        """Tests that the ValueError is raised on invalid input."""
        with self.assertRaises(ValueError):
            sentence_list = [1, "Here we go again"]
            batch_remove_non_ascii_characters(sentence_list)

    def test_batch_replace_contractions_raises_ValueError(self):
        """Tests that the ValueError is raised on invalid input."""
        with self.assertRaises(ValueError):
            sentence_list = [1, "Here we go again"]
            batch_replace_contractions(sentence_list)

    def test_batch_remove_stopwords_raises_ValueError(self):
        """Tests that the ValueError is raised on invalid input."""
        with self.assertRaises(ValueError):
            sentence_list = [1, "Here we go again"]
            batch_remove_stopwords(sentence_list)

    def test_batch_stem_raises_ValueError(self):
        """Tests that the ValueError is raised on invalid input."""
        with self.assertRaises(ValueError):
            sentence_list = [1, "Here we go again"]
            batch_stem(sentence_list)

    def test_batch_lemmatize_raises_ValueError(self):
        """Tests that the ValueError is raised on invalid input."""
        with self.assertRaises(ValueError):
            sentence_list = [1, "Here we go again"]
            batch_lemmatize(sentence_list)

    def test_batch_remove_punctuation_raises_ValueError(self):
        """Tests that the ValueError is raised on invalid input."""
        with self.assertRaises(ValueError):
            sentence_list = [1, "Here we go again"]
            batch_remove_punctuation(sentence_list)

    def test_batch_remove_numbers_raises_ValueError(self):
        """Tests that the ValueError is raised on invalid input."""
        with self.assertRaises(ValueError):
            sentence_list = [1, "Here we go again"]
            batch_remove_numbers(sentence_list)

    def test_batch_remove_whitespace_raises_ValueError(self):
        """Tests that the ValueError is raised on invalid input."""
        with self.assertRaises(ValueError):
            sentence_list = [1, "Here we go again"]
            batch_remove_whitespace(sentence_list)

    def test_batch_convert_numerals_to_numbers_raises_ValueError(self):
        """Tests that the ValueError is raised on invalid input."""
        with self.assertRaises(ValueError):
            sentence_list = [1, "Here we go again"]
            batch_convert_numerals_to_numbers(sentence_list)

    def test_batch_lowercase_text(self):
        """Processes the sentences in a concurrent manner and converts all
        characters to lowercase."""

        with open(SENTENCES_TITLECASE, encoding="utf-8") as f1:
            sentence_list = json.load(f1)
        with open(SENTENCES_LOWERCASE, encoding="utf-8") as f2:
            expected_result = json.load(f2)
        result = batch_lowercase_text(sentence_list)
        self.assertEqual(expected_result, result)

    def test_batch_remove_non_ascii_characters(self):
        """Processes the sentences in a concurrent manner and removes all
        non-ascii characters"""
        sentences_non_ascii = [
            "6 Ekstr\xf8m",
            "J\xf6reskog bi\xdfchen Z\xfcrcher",
            "\x95He said, \x93Gross, I am going to!\x94",
            "This is a \xA9 but not a \xAE",
            "fractions \xBC, \xBD, \xBE",
            "cows go \xB5",
            "30\xA2",
            "Good bye in Swedish is Hej d\xe5",
            "6\xc2\xa0918\xc2\xa0417\xc2\xa0712",
            "I have 125&#163; on my account",
            "https://sitebulb.com/Folder/øê.html?大学",
            "https://sitebulb.com/Folder/中央大学.html",
        ]
        expected_result = [
            "6 Ekstrm",
            "Jreskog bichen Zrcher",
            "He said, Gross, I am going to!",
            "This is a  but not a ",
            "fractions , , ",
            "cows go ",
            "30",
            "Good bye in Swedish is Hej d",
            "6918417712",
            "I have 125&#163; on my account",
            "https://sitebulb.com/Folder/.html?",
            "https://sitebulb.com/Folder/.html",
        ]
        result = batch_remove_non_ascii_characters(sentences_non_ascii)
        self.assertEqual(expected_result, result)

    def test_batch_remove_stopwords(self):
        """Processes the nested token lists in a concurrent manner and removes
        all stopwords"""
        with open(TOKENS_LOWERCASE, encoding="utf-8") as f1:
            lowercase_tokens = json.load(f1)
        with open(TOKENS_WITHOUT_STOPWORDS, encoding="utf-8") as f2:
            expected_result = json.load(f2)
        result = batch_remove_stopwords(lowercase_tokens)
        self.assertEqual(expected_result, result)

    def test_batch_replace_contractions(self):
        """Processes the sentences in a concurrent manner and resolves
        the contractions."""

        with open(SENTENCES_WITH_CONTRACTIONS, encoding="utf-8") as f:
            sentence_list = json.load(f)
        with open(SENTENCES_WITHOUT_CONTRACTIONS, encoding="utf-8") as f:
            expected_result = json.load(f)
        result = batch_replace_contractions(sentence_list)
        self.assertEqual(expected_result, result)

    def test_batch_stem(self):
        """Processes the nested token lists in a concurrent manner and stems
        all tokens to their root form"""
        with open(TOKENS_LOWERCASE, encoding="utf-8") as f1:
            lowercase_tokens = json.load(f1)
        with open(TOKENS_STEMMED, encoding="utf-8") as f2:
            expected_result = json.load(f2)
        result = batch_stem(
            lowercase_tokens,
            stemmer_type="nltk_regexp_stemmer",
            regexp="ing$|s$|e$|able$",
            min=5,
        )
        self.assertEqual(expected_result, result)

    def test_batch_lemmatize(self):
        """Processes the nested token lists in a concurrent manner and lemmatizes
        all 'verbs'"""
        with open(TOKENS_LOWERCASE, encoding="utf-8") as f1:
            lowercase_tokens = json.load(f1)
        with open(TOKENS_LEMMATIZED, encoding="utf-8") as f2:
            expected_result = json.load(f2)
        result = batch_lemmatize(lowercase_tokens, pos="v")
        self.assertEqual(expected_result, result)

    def test_batch_remove_punctiation(self):
        """Processes the sentences in a concurrent manner and removes all
        punctuation characters."""
        with open(SENTENCES_TITLECASE, encoding="utf-8") as f1:
            sentence_list = json.load(f1)
        with open(SENTENCES_TITLECASE_NO_PUNCT, encoding="utf-8") as f2:
            expected_result = json.load(f2)
        result = batch_remove_punctuation(sentence_list)
        self.assertEqual(expected_result, result)

    def test_batch_remove_numbers(self):
        """Processes the sentences in a concurrent manner and removes all
        numbers."""
        with open(SENTENCES_WITH_NUMBERS, encoding="utf-8") as f1:
            sentence_list = json.load(f1)
        with open(SENTENCES_WITHOUT_NUMBERS, encoding="utf-8") as f2:
            expected_result = json.load(f2)
        result = batch_remove_numbers(sentence_list)
        self.assertEqual(expected_result, result)

    def test_batch_remove_whitespace(self):
        """Processes the sentences in a concurrent manner and removes all
        whitespace characters."""
        with open(SENTENCES_WITH_WHITESPACE, encoding="utf-8") as f1:
            sentence_list = json.load(f1)
        with open(SENTENCES_WITHOUT_WHITESPACE, encoding="utf-8") as f2:
            expected_result = json.load(f2)
        result = batch_remove_whitespace(sentence_list)
        self.assertEqual(expected_result, result)

    def test_batch_convert_numerals_to_digits(self):
        """Processes the sentences in a concurrent manner and replaces all
        numerals with numbers."""
        with open(SENTENCES_WITH_NUMERALS, encoding="utf-8") as f1:
            sentence_list = json.load(f1)
        with open(SENTENCES_WITHOUT_NUMERALS, encoding="utf-8") as f2:
            expected_result = json.load(f2)
        result = batch_convert_numerals_to_numbers(sentence_list)
        self.assertEqual(expected_result, result)
